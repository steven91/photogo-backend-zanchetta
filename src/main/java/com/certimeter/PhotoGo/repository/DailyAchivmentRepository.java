package com.certimeter.PhotoGo.repository;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.certimeter.PhotoGo.mapper.DailyAchivmentMapper;
import com.certimeter.PhotoGo.model.DailyAchivmentModel;
import com.certimeter.PhotoGo.model.TargetModel;

@Repository
public class DailyAchivmentRepository {

	@Autowired
	DailyAchivmentMapper dailyAchivmentMapper;
	
	public String getTarget(int idDailyAchivment) {
		return dailyAchivmentMapper.queryGetTarget(idDailyAchivment);
	}
	
	public ArrayList<DailyAchivmentModel> getAchivment()
	{
		return dailyAchivmentMapper.queryGetAchivment();
	}
	
}
