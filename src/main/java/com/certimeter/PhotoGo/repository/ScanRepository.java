package com.certimeter.PhotoGo.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.certimeter.PhotoGo.mapper.ScanMapper;
import com.certimeter.PhotoGo.request.Scan;

@Repository
public class ScanRepository {
	
	@Autowired
	ScanMapper scanmapper;
	
	public int postScan(Scan photo) 
	{
		return scanmapper.queryInsertNewScan(photo);
	}

}
