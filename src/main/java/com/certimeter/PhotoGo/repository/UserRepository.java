package com.certimeter.PhotoGo.repository;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.certimeter.PhotoGo.dto.UserDto;
import com.certimeter.PhotoGo.mapper.UserMapper;
import com.certimeter.PhotoGo.model.UserModel;
import com.certimeter.PhotoGo.request.Login;
import com.certimeter.PhotoGo.request.Registration;

@Repository
public class UserRepository {

	@Autowired
	UserMapper usermapper;
	
	public UserModel getUserId(int id) {
		return usermapper.queryGetUserByIdUser(id);
	}
	
	public UserModel postLogin(Login user) 
	{
		return usermapper.queryInsertNewLogin(user);
	}
	
	public UserModel checkUsernameEmail(Registration user)
	{
		return usermapper.queryCheckUsernameEmail(user);
	}
	
	public int postRegistration(Registration user)
	{
		return usermapper.queryInsertNewUser(user);
	}
	
	public ArrayList<UserModel> getRanking()
	{
		return usermapper.queryGetRanking();
	}
	
	public int putUser(UserModel user) 
	{
		return usermapper.queryUpdateScore(user);
	}
}