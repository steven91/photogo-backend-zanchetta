
package com.certimeter.PhotoGo.model;

import com.certimeter.PhotoGo.dto.UserDto;

public class UserModel {

	//|||||||||||||||||||||||||||||||||| VARIABLES
	
	private int idUser;
	private String username;
	private String password;
	private String email;
	private int score;
	
	//|||||||||||||||||||||||||||||||||| CONSTRUCTOR
	
	public UserModel() {
		
	}
	
	public UserModel(int idUser, String username, String password, String email, int score) {
		this.idUser = idUser;
		this.username = username;
		this.password = password;
		this.email = email;
		this.score = score;
	}

	//|||||||||||||||||||||||||||||||||| GET e SET
	
	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	//|||||||||||||||||||||||||||||||||| ToSTRING

	@Override
	public String toString() {
		return "UserModel [idUser=" + idUser + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", score=" + score + "]";
	}

	//|||||||||||||||||||||||||||||||||| FROM MODEL TO DTO (CREAZIONE USER_DTO)
    
    public UserDto translateFromModelToDto() {
    	
    	UserDto userDto = new UserDto();
    	
    	userDto.setIdUser(idUser);
    	userDto.setUsername(username);
    	userDto.setEmail(email);
    	userDto.setScore(score);
    	
    	return userDto;
    }
}
