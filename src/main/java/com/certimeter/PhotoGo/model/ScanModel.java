package com.certimeter.PhotoGo.model;

import java.time.OffsetDateTime;

public class ScanModel {
	
	//|||||||||||||||||||||||||||||||||| VARIABLES

	private int idScan;
	private int idUserFk;
	private OffsetDateTime data;
	private int idDailyAchivmentHistoryFk;
	private int confidence;
	
	//|||||||||||||||||||||||||||||||||| CONSTRUCTOR

	public ScanModel() {
		
	}
	
	public ScanModel(int idScan, int idUserFk, OffsetDateTime data, int idDailyAchivmentHistoryFk, int confidence) {
		this.idScan = idScan;
		this.idUserFk = idUserFk;
		this.data = data;
		this.idDailyAchivmentHistoryFk = idDailyAchivmentHistoryFk;
		this.confidence = confidence;
	}

	//|||||||||||||||||||||||||||||||||| GET e SET
	
	public int getIdScan() {
		return idScan;
	}

	public void setIdScan(int idScan) {
		this.idScan = idScan;
	}

	public int getIdUserFk() {
		return idUserFk;
	}

	public void setIdUserFk(int idUserFk) {
		this.idUserFk = idUserFk;
	}

	public OffsetDateTime getData() {
		return data;
	}

	public void setData(OffsetDateTime data) {
		this.data = data;
	}

	public int getIdDailyAchivmentHistoryFk() {
		return idDailyAchivmentHistoryFk;
	}

	public void setIdDailyAchivmentHistoryFk(int idDailyAchivmentHistoryFk) {
		this.idDailyAchivmentHistoryFk = idDailyAchivmentHistoryFk;
	}

	public int getConfidence() {
		return confidence;
	}

	public void setConfidence(int confidence) {
		this.confidence = confidence;
	}

	//|||||||||||||||||||||||||||||||||| ToSTRING
	
	@Override
	public String toString() {
		return "ScanModel [idScan=" + idScan + ", idUserFk=" + idUserFk + ", data=" + data
				+ ", idDailyAchivmentHistoryFk=" + idDailyAchivmentHistoryFk + ", confidence=" + confidence + "]";
	}
	
	
	
	
	

}
