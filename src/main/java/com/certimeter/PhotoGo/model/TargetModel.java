package com.certimeter.PhotoGo.model;

public class TargetModel {
	
	//|||||||||||||||||||||||||||||||||| VARIABLES
	
	private int idDailyAchivment;
	private String targetObject;

	//|||||||||||||||||||||||||||||||||| CONSTRUCTOR
	
	public TargetModel() {
		
	}

	public TargetModel(int idDailyAchivment, String targetObject) {
		this.idDailyAchivment = idDailyAchivment;
		this.targetObject = targetObject;
	}
	
	//|||||||||||||||||||||||||||||||||| GET e SET

	public int getIdDailyAchivment() {
		return idDailyAchivment;
	}

	public void setIdDailyAchivment(int idDailyAchivment) {
		this.idDailyAchivment = idDailyAchivment;
	}

	public String getTargetObject() {
		return targetObject;
	}

	public void setTargetObject(String targetObject) {
		this.targetObject = targetObject;
	}
	
	//|||||||||||||||||||||||||||||||||| ToSTRING
	
	@Override
	public String toString() {
		return "TargetModel [idDailyAchivment=" + idDailyAchivment + ", targetObject=" + targetObject + "]";
	}
		

}
