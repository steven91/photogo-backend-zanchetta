package com.certimeter.PhotoGo.model;

import java.util.Date;

import com.certimeter.PhotoGo.dto.DailyAchivmentDto;

public class DailyAchivmentModel {
	
	//|||||||||||||||||||||||||||||||||| VARIABLES

	private int idDailyAchivment;
	private String targetObject;
	private int idDailyAchivmentHistory;
	private int idDailyAchivmentFk;
	private Date date;
	
	//|||||||||||||||||||||||||||||||||| CONSTRUCTOR

	public DailyAchivmentModel() {
		
	}

	public DailyAchivmentModel(int idDailyAchivment, String targetObject, int idDailyAchivmentHistory,
			int idDailyAchivmentFk, Date date) {
		this.idDailyAchivment = idDailyAchivment;
		this.targetObject = targetObject;
		this.idDailyAchivmentHistory = idDailyAchivmentHistory;
		this.idDailyAchivmentFk = idDailyAchivmentFk;
		this.date = date;
	}
	
	//|||||||||||||||||||||||||||||||||| GET e SET

	public int getIdDailyAchivment() {
		return idDailyAchivment;
	}

	public void setIdDailyAchivment(int idDailyAchivment) {
		this.idDailyAchivment = idDailyAchivment;
	}

	public String getTargetObject() {
		return targetObject;
	}

	public void setTargetObject(String targetObject) {
		this.targetObject = targetObject;
	}

	public int getIdDailyAchivmentHistory() {
		return idDailyAchivmentHistory;
	}

	public void setIdDailyAchivmentHistory(int idDailyAchivmentHistory) {
		this.idDailyAchivmentHistory = idDailyAchivmentHistory;
	}

	public int getIdDailyAchivmentFk() {
		return idDailyAchivmentFk;
	}

	public void setIdDailyAchivmentFk(int idDailyAchivmentFk) {
		this.idDailyAchivmentFk = idDailyAchivmentFk;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	//|||||||||||||||||||||||||||||||||| ToSTRING

	@Override
	public String toString() {
		return "DailyAchivmentModel [idDailyAchivment=" + idDailyAchivment + ", targetObject=" + targetObject
				+ ", idDailyAchivmentHistory=" + idDailyAchivmentHistory + ", idDailyAchivmentFk=" + idDailyAchivmentFk
				+ ", date=" + date + "]";
	}
	
	public DailyAchivmentDto translateFromModelToDto() {
		
		DailyAchivmentDto dailyAchivmentDto = new DailyAchivmentDto();
		
		dailyAchivmentDto.setIdDailyAchivment(idDailyAchivment);
		dailyAchivmentDto.setTargetObject(targetObject);
		
		return dailyAchivmentDto;
		
		
	}
		
}
