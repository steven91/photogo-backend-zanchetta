package com.certimeter.PhotoGo.model;

public class ScanDetailModel {
	
	//|||||||||||||||||||||||||||||||||| VARIABLES
	
	private int idScanDetail;
	private int idScanFk;
	private String objectLabel;
	private int confidence;
	
	//|||||||||||||||||||||||||||||||||| CONSTRUCTOR
	
	public ScanDetailModel() {
		
	}
	
	public ScanDetailModel(int idScanDetail, int idScanFk, String objectLabel, int confidence) {
		this.idScanDetail = idScanDetail;
		this.idScanFk = idScanFk;
		this.objectLabel = objectLabel;
		this.confidence = confidence;
	}
	
	//|||||||||||||||||||||||||||||||||| GET e SET

	public int getIdScanDetail() {
		return idScanDetail;
	}

	public void setIdScanDetail(int idScanDetail) {
		this.idScanDetail = idScanDetail;
	}

	public int getIdScanFk() {
		return idScanFk;
	}

	public void setIdScanFk(int idScanFk) {
		this.idScanFk = idScanFk;
	}

	public String getObjectLabel() {
		return objectLabel;
	}

	public void setObjectLabel(String objectLabel) {
		this.objectLabel = objectLabel;
	}

	public int getConfidence() {
		return confidence;
	}

	public void setConfidence(int confidence) {
		this.confidence = confidence;
	}

	//|||||||||||||||||||||||||||||||||| ToSTRING
	
	@Override
	public String toString() {
		return "ScanDetailModel [idScanDetail=" + idScanDetail + ", idScanFk=" + idScanFk + ", objectLabel="
				+ objectLabel + ", confidence=" + confidence + "]";
	}
	
}
