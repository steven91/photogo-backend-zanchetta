package com.certimeter.PhotoGo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.certimeter.PhotoGo.dto.UserDto;
import com.certimeter.PhotoGo.model.UserModel;
import com.certimeter.PhotoGo.repository.UserRepository;
import com.certimeter.PhotoGo.request.Login;
import com.certimeter.PhotoGo.request.Registration;

@Service
public class UserService {

	@Autowired
	UserRepository repository;
	
	public UserModel getUserId(int id) {
		return repository.getUserId(id);
	}
	
	public UserModel postLogin(Login user) 
	{
		return repository.postLogin(user);
	}
	
	public UserModel checkUsernameEmail(Registration user)
	{
		return repository.checkUsernameEmail(user);
	}
	
	public int postRegistration(Registration user)
	{
		return repository.postRegistration(user);
	}
	
	public ArrayList<UserModel> getRanking()
	{
		return repository.getRanking();
	}
	
	public int putUser(UserModel user) 
	{
		return repository.putUser(user);
	}
	
	
}
