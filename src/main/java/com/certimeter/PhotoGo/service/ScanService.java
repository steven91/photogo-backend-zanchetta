package com.certimeter.PhotoGo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.certimeter.PhotoGo.repository.ScanRepository;
import com.certimeter.PhotoGo.request.Scan;

@Service
public class ScanService {

	@Autowired
	ScanRepository repository;

	public int postScan(Scan photo) 
	{
		return repository.postScan(photo);
	}
}
