package com.certimeter.PhotoGo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.certimeter.PhotoGo.model.DailyAchivmentModel;
import com.certimeter.PhotoGo.model.TargetModel;
import com.certimeter.PhotoGo.model.UserModel;
import com.certimeter.PhotoGo.repository.DailyAchivmentRepository;

@Service
public class DailyAchivmentService {
	
	@Autowired
	DailyAchivmentRepository repositoryDailtAchivment;

	public String getTarget(int idDailyAchivment) {
		return repositoryDailtAchivment.getTarget(idDailyAchivment);
	}
	
	public ArrayList<DailyAchivmentModel> getAchivment()
	{
		return repositoryDailtAchivment.getAchivment();
	}
}
