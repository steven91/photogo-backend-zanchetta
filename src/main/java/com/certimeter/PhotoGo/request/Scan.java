package com.certimeter.PhotoGo.request;

import java.util.Date;

public class Scan {

	//|||||||||||||||||||||||||||||||||| VARIABLES

	private String labelObject;
	private float confidence;
	
	//|||||||||||||||||||||||||||||||||| CONSTRUCTOR

	public Scan() {
		
	}
	
	public Scan(String labelObject, float confidence) {
		this.labelObject = labelObject;
		this.confidence = confidence;
	}

	//|||||||||||||||||||||||||||||||||| GET e SET

	public String getLabelObject() {
		return labelObject;
	}

	public void setLabelObject(String labelObject) {
		this.labelObject = labelObject;
	}

	public float getConfidence() {
		return confidence;
	}

	public void setConfidence(float confidence) {
		this.confidence = confidence;
	}
	
	//|||||||||||||||||||||||||||||||||| ToSTRING
	
	@Override
	public String toString() {
		return "Scan [labelObject=" + labelObject + ", confidence=" + confidence + "]";
	}
	
	public int translateConfidenceToScore()
	{
		int percentage = (int)(confidence * 100);
		int pt = 0;
		
		if(percentage < 100 && percentage > 90)
		{
			pt = 100;
		}
		else if(percentage < 89 && percentage > 80)
		{
			pt = 80;
		}
		else if(percentage < 79 && percentage > 60)
		{
			pt = 60;
		}
		else //confidence <= 59
		{
			pt = 0;
		}
		return pt;
	}

}
