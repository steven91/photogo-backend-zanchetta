package com.certimeter.PhotoGo.request;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.certimeter.PhotoGo.dto.UserDto;


public class Login {
	
	//|||||||||||||||||||||||||||||||||| VARIABLES
	
	private String password;
	private String email;
	
	//|||||||||||||||||||||||||||||||||| CONSTRUCTOR

	public Login() {
		
	}

	public Login(String password, String email) {
		this.password = password;
		this.email = email;
	}

	//|||||||||||||||||||||||||||||||||| GET e SET
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	//|||||||||||||||||||||||||||||||||| ToSTRING

	@Override
	public String toString() {
		return "LoginModel [password=" + password + ", email=" + email + "]";
	}
	
	public boolean checkFields() {
		
		String emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z]+\\.[A-Za-z]{2,5}";
		String passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";
				
		Pattern patternEmail = Pattern.compile(emailRegex);
		Pattern patternPassword = Pattern.compile(passwordRegex);
		
		Matcher matcherEmail = patternEmail.matcher(email);
		Matcher matcherPassword = patternPassword.matcher(password);
		
		boolean matchesEmail = matcherEmail.matches();
		boolean matchesPassword = matcherPassword.matches();
		
		return (matchesEmail && matchesPassword);
					
	}
}
