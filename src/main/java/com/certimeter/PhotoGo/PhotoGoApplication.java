package com.certimeter.PhotoGo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotoGoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotoGoApplication.class, args);
	}

}
