package com.certimeter.PhotoGo.dto;

public class DailyAchivmentDto {
	
	private int idDailyAchivment;
	private String targetObject;
	
	public DailyAchivmentDto() {
		
	}

	public DailyAchivmentDto(int idDailyAchivment, String targetObject) {
		this.idDailyAchivment = idDailyAchivment;
		this.targetObject = targetObject;
	}

	public int getIdDailyAchivment() {
		return idDailyAchivment;
	}

	public void setIdDailyAchivment(int idDailyAchivment) {
		this.idDailyAchivment = idDailyAchivment;
	}

	public String getTargetObject() {
		return targetObject;
	}

	public void setTargetObject(String targetObject) {
		this.targetObject = targetObject;
	}

	@Override
	public String toString() {
		return "DailyAchivmentDto [idDailyAchivment=" + idDailyAchivment + ", targetObject=" + targetObject + "]";
	}
	
	
}
