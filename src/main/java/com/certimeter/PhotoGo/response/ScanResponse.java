package com.certimeter.PhotoGo.response;

public class ScanResponse {

	//|||||||||||||||||||||||||||||||||| VARIABLES

	private boolean target;
	private int score;
	
	//|||||||||||||||||||||||||||||||||| CONSTRUCTOR

	public ScanResponse() {
		
	}
	
	//|||||||||||||||||||||||||||||||||| GET e SET

	public ScanResponse(boolean target, int score) {
		this.target = target;
		this.score = score;
	}

	public boolean getTarget() {
		return target;
	}

	public void setTarget(boolean target) {
		this.target = target;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	//|||||||||||||||||||||||||||||||||| ToSTRING

	@Override
	public String toString() {
		return "ScanResponse [target=" + target + ", score=" + score + "]";
	}
	
}
