package com.certimeter.PhotoGo.mapper;

import java.util.ArrayList;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.certimeter.PhotoGo.model.DailyAchivmentModel;
import com.certimeter.PhotoGo.model.TargetModel;
import com.certimeter.PhotoGo.model.UserModel;

@Mapper
public interface DailyAchivmentMapper {

	@Select("SELECT targetObject FROM dailyAchivment WHERE idDailyAchivment = 1")
	//@Select("SELECT targetObject FROM dailyAchivment")
	String queryGetTarget(@PathParam("idDailyAchivment") final int idDailyAchivment);
	
	@Select("SELECT * FROM dailyAchivment")
	ArrayList<DailyAchivmentModel> queryGetAchivment();
	
}
