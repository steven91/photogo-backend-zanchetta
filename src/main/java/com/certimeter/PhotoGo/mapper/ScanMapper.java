package com.certimeter.PhotoGo.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.certimeter.PhotoGo.request.Scan;

@Mapper
public interface ScanMapper {
	
	@Insert("INSERT INTO scan(confidence) VALUES (#{confidence})")
	@Options(useGeneratedKeys = true, keyColumn = "idScan")	
	int queryInsertNewScan(Scan photo);
	// 1 -> inserimento andato a buon fine

}
