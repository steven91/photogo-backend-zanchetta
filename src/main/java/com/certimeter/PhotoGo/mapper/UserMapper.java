package com.certimeter.PhotoGo.mapper;

import java.util.ArrayList;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.certimeter.PhotoGo.dto.UserDto;
import com.certimeter.PhotoGo.model.UserModel;
import com.certimeter.PhotoGo.request.Login;
import com.certimeter.PhotoGo.request.Registration;


@Mapper
public interface UserMapper {
	
	@Select("SELECT * FROM user WHERE idUser=#{idUser}")
	UserModel queryGetUserByIdUser(@PathParam("idUser") final int idUser);

	@Select("SELECT * FROM user WHERE password=#{password} AND email=#{email}")
	UserModel queryInsertNewLogin(Login user);
	
	@Select("SELECT * FROM user WHERE username=#{username} OR email=#{email}")
	UserModel queryCheckUsernameEmail(Registration user);
	
	@Insert("INSERT INTO user(username, password, email) VALUES (#{username}, #{password}, #{email})")
	@Options(useGeneratedKeys = true, keyColumn = "idUser")	
	int queryInsertNewUser(Registration user);
	//se 1: inserimento riuscito
	
	@Select("SELECT * FROM user ORDER BY score DESC")
	ArrayList<UserModel> queryGetRanking();
	
	@Update("UPDATE user SET username=#{username}, password=#{password}, email=#{email}, score=#{score} WHERE idUser=#{idUser}")
	int queryUpdateScore(UserModel user);
	//se 1: update riuscito
}
