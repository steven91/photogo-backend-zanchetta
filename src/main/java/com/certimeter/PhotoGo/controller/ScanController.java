package com.certimeter.PhotoGo.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.certimeter.PhotoGo.dto.UserDto;
import com.certimeter.PhotoGo.model.UserModel;
import com.certimeter.PhotoGo.request.Login;
import com.certimeter.PhotoGo.request.Scan;
import com.certimeter.PhotoGo.response.Response;
import com.certimeter.PhotoGo.response.ScanResponse;
import com.certimeter.PhotoGo.service.DailyAchivmentService;
import com.certimeter.PhotoGo.service.ScanService;
import com.certimeter.PhotoGo.service.UserService;

/*
 * CODE |  MESSAGE
 *      |
 *   0  |  success
 *   1  |  unsuccess
 *  -1  |  server error // error
 *      |
 */

@RestController
@RequestMapping("/scan")
public class ScanController {
	
	@Autowired
	ScanService scanService;
	
	@Autowired
	DailyAchivmentService dailyAchivmentService;
	
	@Autowired
	UserService userService;
	
	@PostMapping("post")
	public @ResponseBody ResponseEntity<Response<ScanResponse>> postScan(@RequestHeader("ID-USER") int idUser, @RequestBody ArrayList<Scan> photos)
	{
		System.out.println("INIZIO -> scan/post");	//CONTROLLO: guardo tra la lista degli oggetti cosa gli passo come parametro SE c'è un'oggetto che combacia con l'OBIETTIVO del giorno
		for(int i = 0; i < photos.size(); i++)
		{			
			//SI -> lo trovo
			if(photos.get(i).getLabelObject().equals(dailyAchivmentService.getTarget(1)))
			{
				//SI' -> lo inserisco nel DB
				//CONTROLLO_2: se è stato inserito
				if(scanService.postScan(photos.get(i)) == 1)
				//SI -> restituisco {target: true, score: aggiornato}
				{
					int point = photos.get(i).translateConfidenceToScore();
					System.out.println("POINT:" + point);
					
					UserModel resultUser = userService.getUserId(idUser);
					resultUser.setScore(resultUser.getScore() + point);
					System.out.println("RESULT USER:" + resultUser);

					
					if(userService.putUser(resultUser) == 1)
					{
						System.out.println("FINE -> scan/post (IF_3)");
						return new ResponseEntity<Response<ScanResponse>>(new Response<ScanResponse>(0, "UPDATE RIUSCITO",new ScanResponse(true, point)), HttpStatus.OK);
					}
					else
					{
						System.out.println("FINE -> scan/post (ELSE_3)");
						return new ResponseEntity<Response<ScanResponse>>(new Response<ScanResponse>(1, "UPDATE FALLITO", new ScanResponse(false, -1)), HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
				//NO -> restituisco {target: false, score: -1}
				else
				{
					System.out.println("FINE -> scan/post  (ELSE_2)");
					return new ResponseEntity<Response<ScanResponse>>(new Response<ScanResponse>(1, "UPDATE FALLITO", new ScanResponse(false, -1)), HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
		}
		//NO -> restituisco {target: false, score: 0}
		System.out.println("FINE -> scan/post (ELSE_più esterno)");
		return new ResponseEntity<Response<ScanResponse>>(new Response<ScanResponse>(1, "TARGET - Non trovato", new ScanResponse(false, 0)),HttpStatus.OK);
	}

}
