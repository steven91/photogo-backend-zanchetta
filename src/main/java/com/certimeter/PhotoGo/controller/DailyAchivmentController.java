package com.certimeter.PhotoGo.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.certimeter.PhotoGo.model.TargetModel;
import com.certimeter.PhotoGo.model.UserModel;
import com.certimeter.PhotoGo.dto.DailyAchivmentDto;
import com.certimeter.PhotoGo.dto.UserDto;
import com.certimeter.PhotoGo.model.DailyAchivmentModel;
import com.certimeter.PhotoGo.response.Response;
import com.certimeter.PhotoGo.service.DailyAchivmentService;

@RestController
@RequestMapping("/dailyAchivment")
public class DailyAchivmentController {

	@Autowired
	DailyAchivmentService dailyAchivmentService;
	
	@GetMapping("get/{idDailyAchivment}")
	public @ResponseBody ResponseEntity<Response<String>> getTarget(@PathVariable int idDailyAchivment)
	{
		if(dailyAchivmentService.getTarget(idDailyAchivment) != null) //ID valid
		{
			return new ResponseEntity<Response<String>>(new Response<String>(0, "ID VALIDO", dailyAchivmentService.getTarget(idDailyAchivment)), HttpStatus.OK);
		}
		else //ID not valid
		{
			return new ResponseEntity<Response<String>>(new Response<String>(1, "ID NON VALIDO", dailyAchivmentService.getTarget(idDailyAchivment)), HttpStatus.OK);
		}	
	}
	
	@GetMapping("get/achivment")
	public @ResponseBody ResponseEntity<Response<ArrayList<DailyAchivmentDto>>> getAchivment() 
	{
		System.out.println("INIZIO get/achivment");
		//|||||||||||||||||||||||||||||||||| TRADURRE I SINGOLI USER DA MODEL A DTO 
		
		ArrayList<DailyAchivmentModel> achivmentList = dailyAchivmentService.getAchivment();
		ArrayList<DailyAchivmentDto> achivmentListDto = new ArrayList<>();

		//|||||||||||||||||||||||||||||||||| INSERIRLI NELL'ARRAY MODIFICATI

		for (int i = 0; i < achivmentList.size(); i++) {
			
			//|||||||||||||||||||||||||||||||||| TRADUZIONE
			DailyAchivmentDto dailyAchivmentDto = achivmentList.get(i).translateFromModelToDto();
			
			//|||||||||||||||||||||||||||||||||| INSERIMENTO NELL'ARRAYLIST DI TIPO USERDTO
			achivmentListDto.add(dailyAchivmentDto);
			
			System.out.println(dailyAchivmentDto);
			
		}
		System.out.println("FINE get/achivment");
	    return new ResponseEntity<Response<ArrayList<DailyAchivmentDto>>>(new Response<ArrayList<DailyAchivmentDto>>(0, "ELENCO OBIETTIVI", achivmentListDto), HttpStatus.OK);
	}
	
}
