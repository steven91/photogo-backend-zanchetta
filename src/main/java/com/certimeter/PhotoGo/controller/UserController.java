package com.certimeter.PhotoGo.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.certimeter.PhotoGo.dto.UserDto;
import com.certimeter.PhotoGo.model.UserModel;
import com.certimeter.PhotoGo.request.Login;
import com.certimeter.PhotoGo.request.Registration;
import com.certimeter.PhotoGo.response.Response;
import com.certimeter.PhotoGo.service.UserService;

/*
 * CODE |  MESSAGE
 *      |
 *   0  |  success
 *   1  |  unsuccess
 *  -1  |  server error // error
 *      |
 */

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	
	@GetMapping("get/{id}")
	public @ResponseBody ResponseEntity<Response<UserDto>> getUserId(@PathVariable int id) 
	{
		System.out.println("INIZIO get({id}");
		if(userService.getUserId(id)!=null)  //ID valid
		{
			System.out.println("FINE get({id} -  ID valido");
	    	return new ResponseEntity<Response<UserDto>>(new Response<UserDto>(0, "ID VALIDO", userService.getUserId(id).translateFromModelToDto()), HttpStatus.OK);
		}
		else //ID not valid
		{
			System.out.println("FINE get({id} -  ID non valido");
			return new ResponseEntity<Response<UserDto>>(new Response<UserDto>(1, "ID NON VALIDO", new UserDto()), HttpStatus.UNAUTHORIZED);
		}
		
	}
	
	@PostMapping("post/login")
	public @ResponseBody ResponseEntity<Response<UserDto>> postLogin(@RequestBody Login login)
	{
		System.out.println("SONO DENTRO IL POST/LOGIN!!!!!!");
		//CONTROLLO_1 : sintassi corretta di password ed email
		if(login.checkFields() == true)
		{	
			System.out.println("IF_1");
		
			UserModel userModel = userService.postLogin(login);
			
			//CONTROLLO_2 : email e password se già esistono
			if(userModel != null)
			{
				System.out.println("IF_2");
				System.out.println(login.toString());
				return new ResponseEntity<Response<UserDto>>(new Response<UserDto>(0, "USER ESISTENTE - User loggato", userModel.translateFromModelToDto()), HttpStatus.OK);
 
			}
			else
			{
				System.out.println("ELSE_2");
				return new ResponseEntity<Response<UserDto>>(new Response<UserDto>(1, "USER INESISTENTE - User non registrato", new UserDto()), HttpStatus.OK);

			}
		}
		else 
		{
			System.out.println("ELSE_1");
			return new ResponseEntity<Response<UserDto>>(new Response<UserDto>(-1, "SINTASSI ERRATA", new UserDto()), HttpStatus.OK);
		}
	}
	
	
	@PostMapping("post/registration")
	public @ResponseBody ResponseEntity<Response<UserDto>> postRegistration(@RequestBody Registration registration)
	{
		
		System.out.println(registration.toString());	

		//CONTROLLO: email, username sintassi corretta 
		if (registration != null && registration.checkFields() == true)
		{
			//CONTROLLO: email, username se già esistono -> GIA' REGISTRATO
			UserModel userModel = userService.checkUsernameEmail(registration);
			//Se ESISTONO
			if(userModel != null)
			{
				System.out.println("ESISTONO GIA'");
				
				UserDto userModelToUserDto  = userModel.translateFromModelToDto();
				return new ResponseEntity<Response<UserDto>>(new Response<UserDto>(1, "USER ESISTENTE - User già registrato", (UserDto)userModelToUserDto), HttpStatus.OK);
	
				//FRONT-END -> ALERT "Sei già registrato!"
			}
			//CONTROLLO: se NON ESISTONO
			else 
			{
				Login login = new Login(registration.getPassword(),registration.getEmail());

				System.out.println("NON ESISTONO GIA'");

				if(userService.postRegistration(registration) == 1)
				{
					Response<UserDto> userDto  = postLogin(login).getBody();
					return new ResponseEntity<Response<UserDto>>(new Response<UserDto>(0, "USER INESISTENTE - User registrato con successo", userDto.getData()), HttpStatus.OK);
				}
				else
				{
					return new ResponseEntity<Response<UserDto>>(new Response<UserDto>(-1, "ERRORI", new UserDto()), HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
		}
		//CONTROLLO: email, username sintassi errata 
		else 
		{
			return new ResponseEntity<Response<UserDto>>(new Response<UserDto>(-1, "SINTASSI ERRATA", new UserDto()), HttpStatus.OK);
		}

	}
	
	@GetMapping("get/ranking")
	public @ResponseBody ResponseEntity<Response<ArrayList<UserDto>>> getRanking()
	{
		
		System.out.println("INIZIO get/ranking");
		//|||||||||||||||||||||||||||||||||| TRADURRE I SINGOLI USER DA MODEL A DTO 
		
		ArrayList<UserModel> list = userService.getRanking();
		ArrayList<UserDto> listDto = new ArrayList<>();

		//|||||||||||||||||||||||||||||||||| INSERIRLI NELL'ARRAY MODIFICATI

		for (int i = 0; i < list.size(); i++) {
			
			//|||||||||||||||||||||||||||||||||| TRADUZIONE
			UserDto userDto = list.get(i).translateFromModelToDto();
			
			//|||||||||||||||||||||||||||||||||| INSERIMENTO NELL'ARRAYLIST DI TIPO USERDTO
			listDto.add(userDto);
			
		}
		System.out.println("FINE get/ranking");
	    return new ResponseEntity<Response<ArrayList<UserDto>>>(new Response<ArrayList<UserDto>>(0, "ELENCO DATI", listDto), HttpStatus.OK);
	}
	
	//Aggiornamento score
	@PutMapping("put/score")
	public @ResponseBody ResponseEntity<Response<String>> putUser(@RequestBody UserModel userModel) 
	{
		System.out.println("INIZIO put/score");
		if(userService.putUser(userModel) == 1)
		{
			System.out.println("FINE put/score IF");
			return new ResponseEntity<Response<String>>(new Response<String>(0, "UPDATE - Update user riuscito", "Succesful update user"), HttpStatus.OK);
		}
		else //==0
		{
			System.out.println("FINE put/score ELSE");
			return new ResponseEntity<Response<String>>(new Response<String>(1, "FAILED UPDATE - Update user non riuscito", "Unsuccesful update user"), HttpStatus.OK);
		}	
	}
	
	
	
}
